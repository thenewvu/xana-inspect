'use strict';

const util = require('util');

/**
 * Default options.
 * @type {{depth: null, maxArrayLength: null}}
 */
const DEFAULT_OPTIONS = {
	depth: null,
	maxArrayLength: null
};

/**
 * Just like util.inspect but with default options:
 * depth=null and maxArrayLength=null.
 * @param obj - The inspected object.
 * @param opts - Optional options.
 * @returns {*}
 */
module.exports = function (obj, opts) {
	opts = Object.assign({}, DEFAULT_OPTIONS, opts);
	return util.inspect(obj, opts);
};
